var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var cors = require("cors");
var mysql = require("mysql");

// setup body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

//connection configuration
var dbConn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "cdap"
});
//create connection with database
dbConn.connect();

//test the api
app.get("/", function(req, res) {
  return res.send({ error: true, message: "hello" });
});

//retrieve all the users
app.get("/users", function(req, res) {
  dbConn.query("SELECT * FROM users", function(error, results, fields) {
    if (error) throw error;
    return res.send({ error: false, data: results, message: "Complete Data" });
  });
});

//retrieve user by username
app.get("/users/getById/:username", function(req, res) {
  let username = req.params.username;
  if (!username) {
    return res
      .status(400)
      .send({ error: true, message: "Please provide username" });
  }
  dbConn.query("SELECT * FROM users WHERE username=?", username, function(
    error,
    results,
    fields
  ) {
    if (error) throw error;
    return res.send({
      error: false,
      data: results[0],
      message: "User by username"
    });
  });
});

//add a new record
app.post("/users/add", function(req, res) {
  let username = req.body.username;
  let password = req.body.password;
  if (!username && !password) {
    return res
      .status(400)
      .send({ error: true, message: "please provide username and password" });
  }
  dbConn.query(
    "INSERT INTO users(id,username,password) VALUE(?,?,?)",
    ["", username, password],
    function(error, results, fields) {
      if (error) throw error;
      return res.send({
        error: false,
        data: results,
        message: "Record has been added"
      });
    }
  );
});

//update user by id
app.put("/users/update", function(req, res) {
  let id = req.body.id;
  let username = req.body.username;
  let password = req.body.password;
  if (!id || !username || !password) {
    return res
      .status(400)
      .send({ error: user, message: "Please provide full information" });
  }
  dbConn.query(
    "UPDATE users SET username=? , password=? WHERE id=?",
    [username, password, id],
    function(error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: "data updated" });
    }
  );
});

//delete user by id
app.delete("/users/delete", function(req, res) {
  let id = req.body.id;

  if (!id) {
    return res.status(400).send({ error: true, message: "Please provide id" });
  }
  dbConn.query("DELETE FROM users WHERE id=?", [id], function(
    error,
    results,
    fields
  ) {
    if (error) throw error;
    return res.send({ error: false, data: results, message: "Record Deleted" });
  });
});
app.listen(3000, function() {
  console.log("node app is running on port 3000");
});

//post arms exercise schedule
app.post("/abs/add", function(req, res) {
  let dataString = req.body.dataString;
  if (!dataString) {
    return res
      .status(400)
      .send({ error: true, message: "please provide work out plan" });
  }
  dbConn.query(
    "INSERT INTO abs_exc(week_id,dataString) VALUE(?,?)",
    ["", dataString],
    function(error, results, fields) {
      if (error) throw error;
      return res.send({
        error: false,
        data: results,
        message: "Record has been added"
      });
    }
  );
});

//post user workout
app.post("/workout/add", function(req, res) {
  let week_id = req.body.week_id;
  let exKey = req.body.exKey;
  let name = req.body.name;
  let imageUrl = req.body.imageUrl;
  let day = req.body.day;
  let category = req.body.category;
  let level = req.body.level;
  let username = req.body.username;
  if (
    !week_id ||
    !exKey ||
    !name ||
    !imageUrl ||
    !day ||
    !category ||
    !level ||
    !username
  ) {
    return res
      .status(400)
      .send({ error: true, message: "please provide work out plan" });
  }
  dbConn.query(
    "INSERT INTO workout(id,week_id,exKey,name,imageUrl,day,category,level,username) VALUE(?,?,?,?,?,?,?,?,?)",
    ["", week_id, exKey, name, imageUrl, day, category, level, username],
    function(error, results, fields) {
      if (error) throw error;
      return res.send({
        error: false,
        data: results,
        message: "Record has been added"
      });
    }
  );
});

//get monday workout plan
app.get("/workout/:day/:category", function(req, res) {
  let day = req.params.day;
  let category = req.params.category;
  if (!day || !category) {
    return res.status(400).send({ error: true, message: "Please provide day" });
  }
  dbConn.query(
    "SELECT * FROM workout  WHERE day=? AND week_id IN (SELECT MAX(week_id) FROM workout WHERE day=? AND category=?)",
    [day, day, category],
    function(error, results, fields) {
      if (error) throw error;
      return res.send({
        error: false,
        data: results,
        message: "work out per day"
      });
    }
  );
});
module.exports = app;
